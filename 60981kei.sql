-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 28, 2021 at 05:15 PM
-- Server version: 8.0.23-0ubuntu0.20.04.1
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `60981kei`
--

-- --------------------------------------------------------

--
-- Table structure for table `friends`
--

CREATE TABLE `friends` (
  `id` int NOT NULL COMMENT 'ID',
  `id_user1` int NOT NULL COMMENT 'SOURCE USER ID',
  `id_user2` int NOT NULL COMMENT 'SECOND USER ID',
  `date` date NOT NULL COMMENT 'FRIEND START DATE'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `friends`
--

INSERT INTO `friends` (`id`, `id_user1`, `id_user2`, `date`) VALUES
(1, 1, 2, '2018-09-04'),
(2, 3, 5, '2015-04-20'),
(3, 4, 2, '2020-12-01'),
(4, 1, 3, '2019-05-29'),
(5, 3, 2, '2016-01-12');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int NOT NULL COMMENT 'ID',
  `name` varchar(255) NOT NULL COMMENT 'Наименование'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`) VALUES
(1, '609-81 ПИ'),
(2, 'Группа№1'),
(3, 'Прогноз погоды'),
(4, 'Новости Сургут'),
(5, 'Лабораторные работы'),
(6, 'Программирование'),
(7, 'PHP'),
(8, 'MySQL'),
(9, 'СурГУ'),
(10, 'Коронавирус.net'),
(11, 'Just group'),
(12, 'Мемы'),
(13, 'C# уроки'),
(14, 'YouTube'),
(15, 'Twitch'),
(16, 'Steam');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int NOT NULL COMMENT 'ID',
  `user_id_from` int NOT NULL COMMENT 'SOURCE USER ID',
  `user_id_to` int NOT NULL COMMENT 'DESTINATION USER ID',
  `text` varchar(255) NOT NULL COMMENT 'TEXT',
  `is_private` tinyint(1) NOT NULL COMMENT 'PRIVACY',
  `date` date NOT NULL COMMENT 'DERAPTURE DATA'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `user_id_from`, `user_id_to`, `text`, `is_private`, `date`) VALUES
(1, 1, 2, 'Привет, как дела?', 1, '2021-02-03'),
(2, 2, 4, 'Hello world!', 1, '2018-09-19'),
(3, 1, 3, 'Как дела?', 0, '2020-08-10'),
(4, 1, 5, 'Ку', 1, '2019-05-31'),
(5, 1, 4, 'Dodo?', 0, '2021-02-14'),
(6, 2, 1, 'Завтра идем в ситик', 0, '2019-09-11'),
(7, 2, 3, 'Занят?', 1, '2018-07-16'),
(8, 1, 2, 'Yes, I\'m a boomer...', 0, '2020-11-22'),
(9, 4, 3, '...', 1, '2020-04-25'),
(10, 2, 5, 'lab3', 1, '2020-12-02');

-- --------------------------------------------------------

--
-- Table structure for table `publication_in_group`
--

CREATE TABLE `publication_in_group` (
  `id` int NOT NULL COMMENT 'ID',
  `id_user` int NOT NULL COMMENT 'USER ID',
  `id_group` int NOT NULL COMMENT 'GROUP ID',
  `Text` varchar(255) NOT NULL COMMENT 'PUBLICATION TEXT'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `publication_in_group`
--

INSERT INTO `publication_in_group` (`id`, `id_user`, `id_group`, `Text`) VALUES
(1, 1, 1, 'Hello world!'),
(2, 3, 1, 'Всем хай'),
(3, 4, 1, 'Dodo?'),
(4, 5, 14, 'Завтра новое видео!'),
(5, 2, 16, 'Продам акк за 1р!');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL COMMENT 'ID',
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Имя',
  `last_name` varchar(20) NOT NULL COMMENT 'Фамилия',
  `middle_name` varchar(20) DEFAULT NULL COMMENT 'Отчество',
  `count_friends` int DEFAULT NULL COMMENT 'Количество друзей'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `last_name`, `middle_name`, `count_friends`) VALUES
(1, 'Егор', 'Куцуев', 'Игоревич', 2),
(2, 'Павел', 'Бардаков', 'Дмитриевич', 3),
(3, 'Алексей', 'Алексеев', 'Вадимович', 3),
(4, 'Николай', 'Сиренко', 'Викторович', 1),
(5, 'Алексей', 'Петров', 'Викторович', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int NOT NULL COMMENT 'ID',
  `id_user` int DEFAULT NULL COMMENT 'USER ID',
  `id_group` int DEFAULT NULL COMMENT 'GROUP ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `id_user`, `id_group`) VALUES
(1, 3, 1),
(2, 1, 1),
(3, 4, 1),
(4, 2, 1),
(5, 3, 13),
(6, 2, 15),
(7, 4, 7),
(8, 5, 16),
(9, 1, 11);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `friends`
--
ALTER TABLE `friends`
  ADD PRIMARY KEY (`id`),
  ADD KEY `First_user_id-connection` (`id_user1`),
  ADD KEY `Second_user_id-connection` (`id_user2`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `User_id_from-connection` (`user_id_from`),
  ADD KEY `User_id_to-connection` (`user_id_to`);

--
-- Indexes for table `publication_in_group`
--
ALTER TABLE `publication_in_group`
  ADD PRIMARY KEY (`id`),
  ADD KEY `User_id-connection` (`id_user`),
  ADD KEY `Group_id-connection` (`id_group`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Users_groups-id_group-connection` (`id_group`),
  ADD KEY `Users_groups-id_user-connection` (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `friends`
--
ALTER TABLE `friends`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `publication_in_group`
--
ALTER TABLE `publication_in_group`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `friends`
--
ALTER TABLE `friends`
  ADD CONSTRAINT `First_user_id-connection` FOREIGN KEY (`id_user1`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `Second_user_id-connection` FOREIGN KEY (`id_user2`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `User_id_from-connection` FOREIGN KEY (`user_id_from`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `User_id_to-connection` FOREIGN KEY (`user_id_to`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `publication_in_group`
--
ALTER TABLE `publication_in_group`
  ADD CONSTRAINT `Group_id-connection` FOREIGN KEY (`id_group`) REFERENCES `groups` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `User_id-connection` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `Users_groups-id_group-connection` FOREIGN KEY (`id_group`) REFERENCES `groups` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `Users_groups-id_user-connection` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
