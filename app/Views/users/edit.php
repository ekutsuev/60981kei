<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container" style="max-width: 540px;">
        <?= form_open_multipart('users/update'); ?>
        <input type="hidden" name="id" value="<?= $users_info["id"] ?>">

        <div class="form-group">
            <label for="name">Имя</label>
            <input type="text" class="form-control <?= ($validation->hasError('name')) ? 'is-invalid' : ''; ?>" name="name"
                   value="<?= $users_info["name"]; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('name') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="surname">Фамилие</label>
            <input type="text" class="form-control <?= ($validation->hasError('surname')) ? 'is-invalid' : ''; ?>" name="surname"
                   value="<?= $users_info["surname"]; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('surname') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="middle_name">Отчество</label>
            <input type="text" class="form-control" name="middle_name"
                   value="<?= $users_info["middle_name"]; ?>">
        </div>
        <input type="hidden" name="count_friends" value="<?= $users_info["count_subscribers"] ?>">
        <input type="hidden" name="count_friends" value="<?= $users_info["count_subscriptions"] ?>">
        <input type="hidden" name="users_id" value="<?= $users_info["users_id"] ?>">
        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="submit">Сохранить</button>
        </div>
        </form>

    </div>
<?= $this->endSection() ?>