<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
    <h2>Подписчики</h2>
    <?php if (!empty($friends) && is_array($friends)) : ?>
        <?php foreach ($friends as $item) : ?>
            <div class="card mb-3">
                <div class="row">
                    <div class="col-md-4 d-flex align-items-center">
                        <?php if (is_null($item['picture_url'])) : ?>
                            <img height="100" width="100" src="/user.svg" class="card-img" alt="<?= esc($item['name1']); ?>">
                        <?php else : ?>
                            <img height="100" src="<?= esc($item['picture_url']); ?>" class="rounded-circle mx-auto" alt="<?= esc($item['name1']); ?>">
                        <?php endif ?>
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title"><?= esc($item['name1']); ?> <?= esc($item['surname1']); ?> <?= esc($item['middle_name1']); ?></h5>
                            <a href="<?= base_url()?>/users/view/<?= esc($item['id_user_from']); ?>" class="btn btn-primary">Профиль</a>
                            <?php if($ionAuth->user()->row()->id == $user['users_id']): ?>
                                <a href="<?= base_url()?>/users/view/<?= esc($item['id_user_from']); ?>" class="btn btn-danger">Удалить</a>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    <?php else : ?>
        <p>Никого нет :(</p>
    <?php endif ?>
</div>
<?= $this->endSection() ?>
