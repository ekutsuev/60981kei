<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
    <h2>Все пользователи</h2>
    <div>
        <?php if (!empty($users) && is_array($users)) : ?>
            <?php foreach ($users as $item): ?>
                <div class="card mb-3" style="max-width: 540px;">
                    <div class="row">
                        <div class="col-md-4 d-flex align-items-center">
                            <?php if (is_null($item['picture_url'])) : ?>
                                <img height="100" width="100" src="/user.svg" class="card-img" alt="<?= esc($item['name']); ?>">
                            <?php else : ?>
                                <img height="100" src="<?= esc($item['picture_url']); ?>" class="rounded-circle mx-auto" alt="<?= esc($item['name']); ?>">
                            <?php endif ?>
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title"><?= esc($item['surname']);?> <?=esc($item['name']);?> <?= esc($item['middle_name'])?></h5>
                                <a href="<?= base_url()?>/users/view/<?= esc($item['id']); ?>" class="btn btn-primary">Профиль</a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php else : ?>
            <p>Пользователи не найдены.</p>
        <?php endif ?>
    </div>
</div>
<?= $this->endSection() ?>

