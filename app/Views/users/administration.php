<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <?php if (!empty($users_info) && is_array($users_info)) : ?>
            <h2>Все пользователи:</h2>
            <div class="d-flex justify-content-between mb-2">
                <?= $pager->links('group1','my_page') ?>
                <?= form_open('users/viewAllWithIon', ['style' => 'display: flex']); ?>
                <select name="per_page" class="ml-3" aria-label="per_page">
                    <option value="2" <?php if($per_page == '2') echo("selected"); ?>>2</option>
                    <option value="5"  <?php if($per_page == '5') echo("selected"); ?>>5</option>
                    <option value="10" <?php if($per_page == '10') echo("selected"); ?>>10</option>
                    <option value="20" <?php if($per_page == '20') echo("selected"); ?>>20</option>
                </select>
                <button class="btn btn-outline-success" type="submit" class="btn btn-primary">На странице</button>
                </form>
                <?= form_open('users/viewAllWithIon',['style' => 'display: flex']); ?>
                <input type="text" class="form-control ml-3" name="search" placeholder="Имя или email" aria-label="Search"
                       value="<?= $search; ?>">
                <button class="btn btn-outline-success" type="submit" class="btn btn-primary">Найти</button>
                </form>
            </div>
            <table class="table table-hover">
                <thead>
                <th scope="col">Аватар</th>
                <th scope="col">ФИО</th>
                <th scope="col">Email</th>
                <th scope="col">Управление</th>
                </thead>
                <tbody>
                <?php foreach ($users_info as $item): ?>
                    <?php if($item['group_id'] == 2) :?>
                        <tr>
                            <td class="align-middle">
                                <div class="col d-flex align-items-center">
                                    <?php if (is_null($item['picture_url'])) : ?>
                                        <img height="100" width="100" src="/user.svg" class="card-img" alt="<?= esc($item['name']); ?>">
                                    <?php else : ?>
                                        <img height="100" src="<?= esc($item['picture_url']); ?>" class="rounded-circle mx-auto" alt="<?= esc($item['name']); ?>">
                                    <?php endif ?>
                                </div>
                            </td>
                            <td class="align-middle">
                                <div>
                                    <?= esc($item['surname']); ?>
                                </div>
                                <div>
                                    <?= esc($item['name']); ?>
                                </div>
                                <div>
                                    <?= esc($item['middle_name']); ?>
                                </div>
                            </td>
                            <td class="align-middle"><?= esc($item['email']); ?></td>
                            <td class="align-middle">
                                <a href="<?= base_url()?>/users/view/<?= esc($item['id']); ?>" class="btn btn-primary btn-sm">Просмотреть</a>
                                <a href="<?= base_url()?>/users/edit/<?= esc($item['id']); ?>" class="btn btn-warning btn-sm">Редактировать</a>
                                <a href="<?= base_url()?>/users/view/<?= esc($item['id']); ?>" class="btn btn-danger btn-sm">Удалить</a>
                            </td>
                        </tr>
                    <?php endif ?>
                <?php endforeach; ?>
                </tbody>
            </table>

        <?php else : ?>
            <div class="text-center">
                <div class="d-flex justify-content-between mb-2">
                    <?= $pager->links('group1','my_page') ?>
                    <?= form_open('users/viewAllWithIon', ['style' => 'display: flex']); ?>
                    <select name="per_page" class="ml-3" aria-label="per_page">
                        <option value="2" <?php if($per_page == '2') echo("selected"); ?>>2</option>
                        <option value="5"  <?php if($per_page == '5') echo("selected"); ?>>5</option>
                        <option value="10" <?php if($per_page == '10') echo("selected"); ?>>10</option>
                        <option value="20" <?php if($per_page == '20') echo("selected"); ?>>20</option>
                    </select>
                    <button class="btn btn-outline-success" type="submit" class="btn btn-primary">На странице</button>
                    </form>
                    <?= form_open('users/viewAllWithIon',['style' => 'display: flex']); ?>
                    <input type="text" class="form-control ml-3" name="search" placeholder="Имя или email" aria-label="Search"
                           value="<?= $search; ?>">
                    <button class="btn btn-outline-success" type="submit" class="btn btn-primary">Найти</button>
                    </form>
                </div>
                <p>Пользователи не найдены </p>
                <a class="btn btn-primary btn-lg" href="<?= base_url()?>/user/create_user"><span class="fas fa-tachometer-alt" style="color:white"></span>&nbsp;&nbsp;Создать пользователя</a>
            </div>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>