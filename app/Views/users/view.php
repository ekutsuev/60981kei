<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
    <?php use CodeIgniter\I18n\Time; ?>
    <?php if (!empty($users) && !empty($users_groups)) : ?>
        <div class="card pt-4 pb-4">
            <div class="row">
                <div class="col-md-4 d-flex align-items-center">
                    <?php if (is_null($users['picture_url'])) : ?>
                        <?php if ($users_groups['group_id'] == 1): ?>
                            <img height="200" width="200" src="/admin.svg" class="card-img" alt="<?= esc($users['name']); ?>">
                        <?php else: ?>
                            <img height="200" width="200" src="/user.svg" class="card-img" alt="<?= esc($users['name']); ?>">
                        <?php endif ?>
                    <?php else : ?>
                        <img height="200" src="<?= esc($users['picture_url']); ?>" class="rounded-circle mx-auto" alt="<?= esc($users['name']); ?>">
                    <?php endif ?>
                </div>
                <div class="col-md-8">
                    <div class="card-body">
                        <h3 class="card-title"><?= esc($users['surname']);?> <?=esc($users['name']);?> <?= esc($users['middle_name'])?></h3>
                        <div>
                            <a type="button" class="btn btn-primary" href="<?= base_url()?>/users/subscribers/<?= esc($users['id']); ?>">
                                Подписчики <span class="badge badge-light"><?=esc($users['count_subscribers'])?></span>
                            </a>
                            <a type="button" class="btn btn-primary" href="<?= base_url()?>/users/subscriptions/<?= esc($users['id']); ?>">
                                Подписки <span class="badge badge-light"><?=esc($users['count_subscriptions'])?></span>
                            </a>
                            <?php if(($ionAuth->isAdmin()) || ($auth_info['id']) == $users['id']):?>
                                <a class="btn btn-primary" href="<?= base_url()?>/users/edit/<?= esc($users['id'])?>">Редактировать</a>
                            <?php endif ?>
                        </div>
                        <div>
                            <?php if (!($auth_info['id'] == $users['id'])):?>
                                <?php if(empty($sub)) : ?>
                                    <a class="btn btn-outline-success mt-3" href="<?= base_url()?>/users/subs/<?= esc($users['id'])?>">Подписаться</a>
                                <?php else : ?>
                                    <a class="btn btn-outline-danger mt-3" href="<?= base_url()?>/users/unsubs/<?= esc($users['id'])?>">Отписаться</a>
                                <?php endif ?>
                            <?php endif ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    <?php else : ?>
        <p>Пользователь не найден.</p>
    <?php endif ?>
</div>
<?= $this->endSection() ?>
