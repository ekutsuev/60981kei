<!DOCTYPE html>
<head>
    <title>Универсальная рейтинговая система CURaing</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/6e9b058a28.js"></script>
    <meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <style>
        main{
            margin-top: 90px;
        }
    </style>
    <link rel="shortcut icon" href="/logo-black.svg">
</head>
<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
    <!--<a class="navbar-brand fa fas fa-tachometer-alt fa-2x" style="color:white" href="<?= base_url()?>"></a>-->
    <a class="mr-3" href="<?= base_url()?>"><img class="" src="/logo-white.svg" alt="logo" width="40" height="40"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item dropdown">
                <?php if ($ionAuth->loggedIn()): ?>
                <a class="nav-link dropdown-toggle active" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Пользователи
                </a>
                <div class="dropdown-menu" aria-labelledby="dropdown01">
                    <a class="dropdown-item" href="<?= base_url()?>/users">Все пользователи</a>
                    <?php if ($ionAuth->isAdmin()): ?>
                        <a class="dropdown-item" href="<?= base_url()?>/users/administration">Администрирование</a>
                    <?php endif ?>
                </div>
                <?php endif ?>
            </li>
            <li class="nav-item dropdown">
                <?php if ($ionAuth->loggedIn()): ?>
                    <a class="nav-link dropdown-toggle active" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Группы
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdown01">
                        <a class="dropdown-item" href="<?= base_url()?>/groups">Все группы</a>
                        <?php if ($ionAuth->isAdmin()): ?>
                            <a class="dropdown-item" href="<?= base_url()?>/groups/administration">Администрирование</a>
                        <?php endif ?>
                    </div>
                <?php endif ?>
            </li>
        </ul>
        <ul class="navbar-nav">
            <?php if (!$ionAuth->loggedIn()): ?>
                <div class="nav-item dropdown">
                    <a class="nav-link active" href="<?= base_url()?>/auth/login"><span class="fas fa fa-sign-in-alt" style="color:white"></span>&nbsp;&nbsp;Вход</a>
                </div>
            <?php else: ?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active text-light" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" ><span class="fas fa fa-user-alt" style="color:white"></span>&nbsp;&nbsp;  <?php echo $ionAuth->user()->row()->email; ?></a>
                    <div class="dropdown-menu" aria-labelledby="dropdown01">
                        <?php if($auth_info): ?>
                            <a class="dropdown-item" href="<?= base_url()?>/users/view/<?= esc($auth_info['id'])?>">Мой профиль</a>
                        <?php endif ?>
                        <a class="dropdown-item" href="<?= base_url()?>/auth/logout"><span class="fas fa fa-sign-in-alt" style="color:white"></span>&nbsp;&nbsp;Выход</a>
                    </div>
                </li>
            <?php endif ?>
        </ul>
    </div>
</nav>
<main role="main">

    <?php if (session()->getFlashdata('message')) :?>
        <div class="alert alert-info" role="alert" style="max-width: 540px;">
            <?= session()->getFlashdata('message') ?>
        </div>
    <?php endif ?>


    <?= $this->renderSection('content') ?>
</main>
<footer class="text-center">
    <p>© Куцуев Егор 2021&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url();?>/pages/view/agreement">Пользовательское соглашение</a></p>
</footer>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>
</html>