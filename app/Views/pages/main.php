<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="jumbotron bg-white text-center">
        <div class="container bg-dark w-25 mb-5 shadow-lg rounded">
            <img class="mb-5 mt-5" src="/logo-white.svg" alt="" width="110" height="110">
        </div>
        <div class="d-flex flex-column" style="max-height: 100%;">
            <div class="container-fluid mb-4">
                <a type="button" class="btn btn-dark btn-lg" href="auth/login">Войти</a>
            </div>
            <div class="container-fluid mb-4">
                <a type="button" class="btn btn-outline-success btn-lg" href="auth/register_user">Создать аккаунт</a>
            </div>
        </div>
    </div>
<?= $this->endSection() ?>