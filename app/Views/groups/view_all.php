<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
    <div class="row mb-3">
        <h2 class="col">Все группы</h2>
        <?php if ($ionAuth->loggedIn()): ?>
            <div class="row justify-content-end align-self-center">
                <a href="<?= base_url()?>/groups/store" class="col btn btn-outline-success">Создать группу</a>
            </div>
        <?php endif ?>
    </div>
    <div>
        <?php if (!empty($groups) && is_array($groups)) : ?>
            <?php foreach ($groups as $item): ?>
                <div class="card mb-3" style="max-width: 580px;">
                    <div class="row">
                        <div class="col-md-4 d-flex align-items-center">
                            <?php if (is_null($item['picture_url'])) : ?>
                                <img height="100" width="100" src="/group.svg" class="rounded-circle mx-auto" alt="<?= esc($item['name']); ?>">
                            <?php else : ?>
                                <img height="100" src="<?= esc($item['picture_url']); ?>" class="rounded-circle mx-auto" alt="<?= esc($item['name']); ?>">
                            <?php endif ?>
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title"><a href="<?= base_url()?>/groups/view/<?= esc($item['id']);?>" class=""> <?=esc($item['name']);?></a></h5>
                                <p>Владелец: <a href="<?= base_url()?>/users/view/<?= esc($item['owner']); ?>"><?= esc($item['user_surname']);?> <?= esc($item['user_name']);?> <?= esc($item['user_middle_name']);?></a></p>
                                <div>
                                    <p>Подписчиков: <?= esc($item['count_subs'])?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php else : ?>
            <p>Группы не найдены(</p>
        <?php endif ?>
    </div>
</div>
<?= $this->endSection() ?>

