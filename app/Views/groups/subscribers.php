<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <h2>Подписчики</h2>
        <?php if (!empty($subscribers) && is_array($subscribers)) : ?>
            <?php foreach ($subscribers as $item) : ?>
                <div class="card mb-3">
                    <div class="row">
                        <div class="col-md-4 d-flex align-items-center">
                            <img height="100" width="100" src="/user.svg" class="card-img" alt="img">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title"><?= $item['user_surname']; ?> <?= esc($item['user_name']); ?> <?= esc($item['user_middle_name']); ?></h5>
                                <a href="<?= base_url()?>/users/view/<?= esc($item['id']); ?>" class="btn btn-primary">Профиль</a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php else : ?>
            <p>Никого нет :(</p>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>