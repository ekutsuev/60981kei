<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container" style="max-width: 540px;">
        <?= form_open_multipart('groups/update'); ?>
        <input type="hidden" name="id" value="<?= $groups["id"] ?>">

        <div class="form-group">
            <label for="birthday">Изображение</label>
            <input type="file" class="form-control-file <?= ($validation->hasError('picture')) ? 'is-invalid' : ''; ?>" name="picture">
            <div class="invalid-feedback">
                <?= $validation->getError('picture') ?>
            </div>
        </div>

        <div class="form-group">
            <label for="name">Название группы</label>
            <input type="text" class="form-control <?= ($validation->hasError('name')) ? 'is-invalid' : ''; ?>" name="name"
                   value="<?= $groups["name"]; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('name') ?>
            </div>

        </div>
        <div class="form-group">
            <label for="description">Описание</label>
            <input type="text" class="form-control <?= ($validation->hasError('description')) ? 'is-invalid' : ''; ?>" name="description"
                   value="<?= $groups["description"] ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('description') ?>
            </div>
        </div>
        <input type="hidden" name="date" value="<?= $groups["date"] ?>">
        <input type="hidden" name="owner" value="<?= $groups["owner"] ?>">
        <input type="hidden" name="count_subs" value="<?= $groups["count_subs"] ?>">
        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="submit">Сохранить</button>
        </div>
        </form>

    </div>
<?= $this->endSection() ?>