<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
    <?php use CodeIgniter\I18n\Time; ?>
    <?php if (!empty($groups)) : ?>
        <div class="card pt-4 pb-4">
            <div class="row">
                <div class="col-md-4 d-flex align-items-center">
                    <?php if (is_null($groups['picture_url'])) : ?>
                        <img height="200" width="200" src="/group.svg" class="rounded-circle mx-auto" alt="<?= esc($groups['name']); ?>">
                    <?php else : ?>
                        <img height="200" src="<?= esc($groups['picture_url']); ?>" class="rounded-circle mx-auto" alt="<?= esc($groups['name']); ?>">
                    <?php endif ?>
                </div>
                <div class="col-md-8">
                    <div class="card-body">
                        <h3 class="card-title"><?=esc($groups['name']);?></h3>
                        <p>Владелец: <a href="<?= base_url()?>/users/view/<?= esc($groups['owner']); ?>"><?= esc($groups['user_surname']);?> <?= esc($groups['user_name']);?> <?= esc($groups['user_middle_name']);?></a></p>
                        <div>
                            <p>Подписчиков: <nobr class="alert alert-success"><?= esc($groups['count_subs'])?></nobr></p>
                        </div>
                        <div class="card-text">
                            <p><?= esc($groups['description'])?></p>
                        </div>
                        <div class="mb-3">
                            <?php if (($auth_info['id'] != $groups['owner'])):?>
                                <?php if(!$isSub) : ?>
                                    <a class="btn btn-outline-success" href="<?= base_url()?>/groups/subs/<?= esc($groups['id'])?>">Подписаться</a>
                                <?php else : ?>
                                    <a class="btn btn-outline-danger" href="<?= base_url()?>/groups/unsubs/<?= esc($groups['id'])?>">Отписаться</a>
                                <?php endif ?>
                            <?php endif ?>
                        </div>
                        <a type="button" class="btn btn-primary" href="<?= base_url()?>/groups/subscribers/<?= esc($groups['id']); ?>">
                            Подписчики <span class="badge badge-light"><?=esc($groups['count_subs'])?></span>
                        </a>
                        <?php if(($ionAuth->isAdmin()) || ($auth_info['id']) == $groups['owner']):?>
                            <a class="btn btn-primary" href="<?= base_url()?>/groups/edit/<?= esc($groups['id'])?>">Редактировать</a>
                            <a class="btn btn-danger" href="<?= base_url()?>/groups/delete/<?= esc($groups['id'])?>">Удалить</a>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
    <?php else : ?>
        <p>Группа не найдена(</p>
    <?php endif ?>
</div>
<?= $this->endSection() ?>
