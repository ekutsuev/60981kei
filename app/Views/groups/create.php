<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container" style="max-width: 540px;">
    <?= form_open_multipart('groups/store'); ?>

    <div class="form-group">
        <label for="birthday">Изображение</label>
        <input type="file" class="form-control-file <?= ($validation->hasError('picture')) ? 'is-invalid' : ''; ?>" name="picture">
        <div class="invalid-feedback">
            <?= $validation->getError('picture') ?>
        </div>
    </div>

    <div class="form-group">
        <label for="name">Название группы</label>
        <input type="text" class="form-control <?= ($validation->hasError('name')) ? 'is-invalid' : ''; ?>" name="name"
               value="<?= old('name'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('name') ?>
        </div>
    </div>

    <div class="form-group">
        <label for="description">Описание</label>
        <input type="text" class="form-control <?= ($validation->hasError('description')) ? 'is-invalid' : ''; ?>" name="description"
               value="<?= old('description'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('description') ?>
        </div>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary" name="submit">Создать</button>
    </div>
    </form>


</div>
<?= $this->endSection() ?>