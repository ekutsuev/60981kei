<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <?php if (!empty($groups) && is_array($groups)) : ?>
            <h2>Все группы:</h2>
            <div class="d-flex justify-content-between mb-2">
                <?= $pager->links('group2','my_page') ?>
                <?= form_open('groups/administration', ['style' => 'display: flex']); ?>
                <select name="per_page" class="ml-3" aria-label="per_page">
                    <option value="2" <?php if($per_page == '2') echo("selected"); ?>>2</option>
                    <option value="5"  <?php if($per_page == '5') echo("selected"); ?>>5</option>
                    <option value="10" <?php if($per_page == '10') echo("selected"); ?>>10</option>
                    <option value="20" <?php if($per_page == '20') echo("selected"); ?>>20</option>
                </select>
                <button class="btn btn-outline-success" type="submit" class="btn btn-primary">На странице</button>
                </form>
                <?= form_open('groups/administration',['style' => 'display: flex']); ?>
                <input type="text" class="form-control ml-3" name="search" placeholder="Назв. Опис. Владелец" aria-label="Search"
                       value="<?= $search; ?>">
                <button class="btn btn-outline-success" type="submit" class="btn btn-primary">Найти</button>
                </form>
            </div>
            <table class="table table-hover">
                <thead>
                <th scope="col">Аватар</th>
                <th scope="col">Название группы</th>
                <th scope="col">Описание</th>
                <th scope="col">Владелец</th>
                <th scope="col">Подписчиков</th>
                <th scope="col">Дата создания</th>
                <th scope="col">Управление</th>
                </thead>
                <tbody>
                <?php foreach ($groups as $item): ?>
                    <tr>
                        <td class="align-middle">
                            <div class="col d-flex align-items-center">
                                <?php if (is_null($item['picture_url'])) : ?>
                                    <img height="100" width="100" src="/group.svg" class="rounded-circle mx-auto" alt="<?= esc($item['name']); ?>">
                                <?php else : ?>
                                    <img height="100" src="<?= esc($item['picture_url']); ?>" class="rounded-circle mx-auto" alt="<?= esc($item['name']); ?>">
                                <?php endif ?>
                            </div>
                        </td>
                        <td class="align-middle">
                            <div>
                                <?= esc($item['name']); ?>
                            </div>
                        </td>
                        <td class="align-middle"><?= esc($item['description']); ?></td>
                        <td class="align-middle">
                            <a href="<?= base_url()?>/users/view/<?= esc($item['owner']); ?>">
                                <div>
                                    <?= esc($item['user_surname']); ?>
                                </div>
                                <div>
                                    <?= esc($item['user_name']); ?>
                                </div>
                                <div>
                                    <?= esc($item['user_middle_name']); ?>
                                </div>
                            </a>
                        </td>
                        <td class="align-middle"><?= esc($item['count_subs']); ?></td>
                        <td class="align-middle"><?= esc($item['date']); ?></td>
                        <td class="align-middle">
                            <a href="<?= base_url()?>/groups/view/<?= esc($item['id']); ?>" class="btn btn-primary btn-sm">Просмотреть</a>
                            <a href="<?= base_url()?>/groups/edit/<?= esc($item['id']); ?>" class="btn btn-warning btn-sm">Редактировать</a>
                            <a href="<?= base_url()?>/groups/view/<?= esc($item['id']); ?>" class="btn btn-danger btn-sm">Удалить</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

        <?php else : ?>
            <div class="text-center">
                <div class="d-flex justify-content-between mb-2">
                    <?= $pager->links('group2','my_page') ?>
                    <?= form_open('groups/administration', ['style' => 'display: flex']); ?>
                    <select name="per_page" class="ml-3" aria-label="per_page">
                        <option value="2" <?php if($per_page == '2') echo("selected"); ?>>2</option>
                        <option value="5"  <?php if($per_page == '5') echo("selected"); ?>>5</option>
                        <option value="10" <?php if($per_page == '10') echo("selected"); ?>>10</option>
                        <option value="20" <?php if($per_page == '20') echo("selected"); ?>>20</option>
                    </select>
                    <button class="btn btn-outline-success" type="submit" class="btn btn-primary">На странице</button>
                    </form>
                    <?= form_open('groups/administration',['style' => 'display: flex']); ?>
                    <input type="text" class="form-control ml-3" name="search" placeholder="Назв. Опис. Владелец" aria-label="Search"
                           value="<?= $search; ?>">
                    <button class="btn btn-outline-success" type="submit" class="btn btn-primary">Найти</button>
                    </form>
                </div>
                <p>Группы не найдены </p>
                <a class="btn btn-primary btn-lg" href="<?= base_url()?>/groups/store"><span class="fas fa-tachometer-alt" style="color:white"></span>&nbsp;&nbsp;Создать группу</a>
            </div>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>