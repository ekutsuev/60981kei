<?php namespace App\Controllers;

use App\Models\GroupsModel;
use App\Models\UsersGroups1Model;
use App\Models\UsersModel;
use Aws\S3\S3Client;

class Groups extends BaseController
{
    public function index() //Отображение всех записей
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/');
        }
        $model = new GroupsModel();
        $data ['groups'] = $model->getGroups();
        echo view('groups/view_all', $this->withIon($data));
    }

    public function view($id) //Отображение одной записи
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/');
        }
        $data = $this->withIon();
        $model = new GroupsModel();
        $userGroups1Model = new UsersGroups1Model();
        $data ['groups'] = $model->getGroups($id);
        $data ['isSub'] = $userGroups1Model->checkSub($id, $data['auth_info']['id']);
        echo view('groups/view', $data);
    }
    public function subscribers($id = null) //Отображение подписчиков
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/');
        }
        $model = new GroupsModel();
        $userGroups1Model = new UsersGroups1Model();
        $data ['groups'] = $model->getGroups($id);
        $data ['subscribers'] = $userGroups1Model->getSubs($id);
        echo view('groups/subscribers', $this->withIon($data));
    }
    public function create()
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $data ['validation'] = \Config\Services::validation();
        echo view('groups/create', $this->withIon($data));
    }
    public function store()
    {
        helper(['form','url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
                'name' => 'required|min_length[3]|max_length[50]',
                'description'  => 'max_length[255]',
                'picture_url' => 'is_image[picture]|max_size[picture,1024]',
            ]))
        {
            $insert = null;
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);

            }
            $model = new GroupsModel();

            if($model->ValidateName($this->request->getPost('name')) == false)
            {
                session()->setFlashdata('message', lang('IonAuth.group_creation_name_exit'));
                return redirect()->to('/groups/create')->withInput();
            }

            $data = [
                'name' => $this->request->getPost('name'),
                'description' => $this->request->getPost('description'),
                'date' => date('Y-m-d'),
                'owner' => (new UsersModel())->getUsersByIonId($this->ionAuth->user()->row()->id)['id'],
                'count_subs' => 1
            ];
            if (!is_null($insert)) $data['picture_url'] = $insert['ObjectURL'];

            $model->save($data);

            $data_auth = $this->withIon();
            $searchGroup = $model->getGroupsByName($data['name']);
            (new UsersGroups1Model())->addSub($searchGroup['id'], $data_auth['auth_info']['id']); //Добавление записи в таблицу groups1

            session()->setFlashdata('message', lang('IonAuth.group_creation_successful'));
            return redirect()->to('/groups');
        }
        else
        {
            return redirect()->to('/groups/create')->withInput();
        }
    }
    public function edit($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new GroupsModel();

        helper(['form']);
        $data ['groups'] = $model->getGroups($id);
        $data ['validation'] = \Config\Services::validation();
        echo view('groups/edit', $this->withIon($data));
    }
    public function update()
    {
        helper(['form','url']);
        echo '/groups/edit/'.$this->request->getPost('id');
        if ($this->request->getMethod() === 'post' && $this->validate([
                'id'  => 'required',
                'name' => 'required|min_length[3]|max_length[50]',
                'description'  => 'max_length[255]',
                'picture_url' => 'is_image[picture]|max_size[picture,1024]'
            ]))
        {
            $insert = null;
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);

            }

            $model = new GroupsModel();
            $data = [
                'id' => $this->request->getPost('id'),
                'name' => $this->request->getPost('name'),
                'description' => $this->request->getPost('description'),
                'date' => $this->request->getPost('date'),
                'owner' => $this->request->getPost('owner'),
                'count_subs' => $this->request->getPost('count_subs')
            ];
            if (!is_null($insert)) $data['picture_url'] = $insert['ObjectURL'];

            $model->save($data);
            //session()->setFlashdata('message', lang('Curating.rating_update_success'));

            return redirect()->to('/groups');
        }
        else
        {
            return redirect()->to('/groups/edit/'.$this->request->getPost('id'))->withInput();
        }
    }

    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new GroupsModel();
        $users_groups1_model = new UsersGroups1Model();
        $users_groups1_model->DeleteUsersGroups1ByGroupId($id);
        $model->delete($id);
        return redirect()->to('/groups');
    }

    public function administration() //Отображение всех пользователей со всей информацией
    {
        if ($this->ionAuth->isAdmin())
        {
            //Подготовка значения количества элементов выводимых на одной странице
            if (!is_null($this->request->getPost('per_page'))) //если кол-во на странице есть в запросе
            {
                //сохранение кол-ва страниц в переменной сессии
                session()->setFlashdata('per_page', $this->request->getPost('per_page'));
                $per_page = $this->request->getPost('per_page');
            }
            else {
                $per_page = session()->getFlashdata('per_page');
                session()->setFlashdata('per_page', $per_page); //пересохранение в сессии
                if (is_null($per_page)) $per_page = '5'; //кол-во на странице по умолчанию
            }
            $data['per_page'] = $per_page;

            //Обработка запроса на поиск
            if (!is_null($this->request->getPost('search')))
            {
                session()->setFlashdata('search', $this->request->getPost('search'));
                $search = $this->request->getPost('search');
            }
            else {
                $search = session()->getFlashdata('search');
                session()->setFlashdata('search', $search);
                if (is_null($search)) $search = '';
            }
            $data['search'] = $search;

            helper(['form','url']);

            $model = new GroupsModel();
            $data['groups'] = $model->getGroupsWithSearch(null, $search)->paginate($per_page, 'group2');
            $data['pager'] = $model->pager;
            echo view('groups/administration', $this->withIon($data));
        }
        else
        {
            //session()->setFlashdata('message', lang('Curating.admin_permission_needed'));
            return redirect()->to('/auth/login');
        }
    }

    public function subs($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $data = $this->withIon();
        $model = new GroupsModel();
        $usersGroups1Model = new UsersGroups1Model();
        $data ['groups'] = $model->getGroups($id);
        //$data ['users_groups'] = $usersGroups1Model->getSubs($data['users']['id']);
        $usersGroups1Model->addSub($data['groups']['id'], $data['auth_info']['id']);
        $model->incrementCountSubscribers($data['groups']['id']);
        return redirect()->to('/groups/view/'.$data['groups']['id']);
    }

    public function unsubs($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $data = $this->withIon();
        $model = new GroupsModel();
        $usersGroups1Model = new UsersGroups1Model();
        $data ['groups'] = $model->getGroups($id);
        //$data ['users_groups'] = $usersGroups1Model->getSubs($data['users']['id']);
        $usersGroups1Model->delSub($data['groups']['id'], $data['auth_info']['id']);
        $model->decrementCountSubscribers($data['groups']['id']);
        return redirect()->to('/groups/view/'.$data['groups']['id']);
    }
}
