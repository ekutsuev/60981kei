<?php namespace App\Controllers;

use App\Models\UsersModel;
use App\Models\FriendsModel;
use App\Models\UsersGroupsModel;

class Users extends BaseController
{
    public function index() //Отображение всех записей
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/');
        }
        $model = new UsersModel();
        $data ['users'] = $model->getUsers();
        echo view('users/view_all', $this->withIon($data));
    }

    public function view($id = null) //Отображение одной записи
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/');
        }
        $model = new UsersModel();
        $groupsModel = new UsersGroupsModel();
        $friendsModel = new FriendsModel();
        $data = $this->withIon();
        $data ['users'] = $model->getUsers($id);
        $data ['users_groups'] = $groupsModel->getGroups($data['users']['users_id']);
        $data ['sub'] = $friendsModel->checkSub($data['auth_info']['id'], $data['users']['id']);
        echo view('users/view', $data);
    }

    public function administration() //Отображение всех пользователей со всей информацией
    {
        if ($this->ionAuth->isAdmin())
        {
            //Подготовка значения количества элементов выводимых на одной странице
            if (!is_null($this->request->getPost('per_page'))) //если кол-во на странице есть в запросе
            {
                //сохранение кол-ва страниц в переменной сессии
                session()->setFlashdata('per_page', $this->request->getPost('per_page'));
                $per_page = $this->request->getPost('per_page');
            }
            else {
                $per_page = session()->getFlashdata('per_page');
                session()->setFlashdata('per_page', $per_page); //пересохранение в сессии
                if (is_null($per_page)) $per_page = '5'; //кол-во на странице по умолчанию
            }
            $data['per_page'] = $per_page;

            //Обработка запроса на поиск
            if (!is_null($this->request->getPost('search')))
            {
                session()->setFlashdata('search', $this->request->getPost('search'));
                $search = $this->request->getPost('search');
            }
            else {
                $search = session()->getFlashdata('search');
                session()->setFlashdata('search', $search);
                if (is_null($search)) $search = '';
            }
            $data['search'] = $search;

            helper(['form','url']);

            $model = new UsersModel();
            $data['users_info'] = $model->getUsersWithIon(null, $search)->paginate($per_page, 'group1');
            $data['pager'] = $model->pager;
            echo view('users/administration', $this->withIon($data));
        }
        else
        {
            //session()->setFlashdata('message', lang('Curating.admin_permission_needed'));
            return redirect()->to('/auth/login');
        }
    }

    public function subscribers($id = null) //Отображение подписчиков
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/');
        }
        $modelFriends = new FriendsModel();
        $modelUsers = new UsersModel();

        $data['friends'] = $modelFriends->getSubscribersWithUsers($id);

        $data['user'] = array();
        $user = $modelUsers->getUsers($id);
        $data['user'] = $user;

        echo view('users/subscribers', $this->withIon($data));
    }

    public function subscriptions($id = null) //Отображение подписчиков
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/');
        }
        $modelFriends = new FriendsModel();
        $modelUsers = new UsersModel();

        $data['friends'] = $modelFriends->getSubscriptionsWithUsers($id);

        $data['user'] = array();
        $user = $modelUsers->getUsers($id);
        $data['user'] = $user;

        echo view('users/subscriptions', $this->withIon($data));
    }

    public function edit($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new UsersModel();

        helper(['form']);
        $data ['users_info'] = $model->getUsers($id);
        $data ['validation'] = \Config\Services::validation();
        echo view('users/edit', $this->withIon($data));
    }
    public function update()
    {
        helper(['form','url']);
        echo '/users_info/edit/'.$this->request->getPost('id');
        if ($this->request->getMethod() === 'post' && $this->validate([
                'id'  => 'required',
                'name' => 'required|min_length[2]|max_length[30]',
                'surname' => 'required|min_length[3]|max_length[20]',
                'middle_name' => 'max_length[20]',
                'count_subscribers'  => 'required',
                'count_subscriptions'  => 'required',
                'users_id'  => 'required'
            ]))
        {
            $model = new UsersModel();
            $model->save([
                'id' => $this->request->getPost('id'),
                'name' => $this->request->getPost('name'),
                'surname' => $this->request->getPost('surname'),
                'middle_name' => $this->request->getPost('middle_name'),
                'count_subscribers' => $this->request->getPost('count_subscribers'),
                'count_subscriptions' => $this->request->getPost('count_subscriptions'),
                'users_id' => $this->request->getPost('users_id')
            ]);
            //session()->setFlashdata('message', lang('Curating.rating_update_success'));

            return redirect()->to('/users/view/'.$this->request->getPost('id'));
        }
        else
        {
            return redirect()->to('/users/edit/'.$this->request->getPost('id'))->withInput();
        }
    }

    public function subs($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new UsersModel();
        $groupsModel = new UsersGroupsModel();
        $friendsModel = new FriendsModel();
        $data = $this->withIon();
        $data ['users'] = $model->getUsers($id);
        $data ['users_groups'] = $groupsModel->getGroups($data['users']['id']);
        $data ['sub'] = $friendsModel->checkSub($data['auth_info']['id'], $data['users']['id']);
        $friendsModel->addSub($data['auth_info']['id'], $data['users']['id']);
        $model->incrementCountSubscribers($data['users']['id']);
        $model->incrementCountSubscriptions($data['auth_info']['id']);
        return redirect()->to('/users/view/'.$data['users']['id']);
    }
    public function unsubs($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new UsersModel();
        $groupsModel = new UsersGroupsModel();
        $friendsModel = new FriendsModel();
        $data = $this->withIon();
        $data ['users'] = $model->getUsers($id);
        $data ['users_groups'] = $groupsModel->getGroups($data['users']['id']);
        $data ['sub'] = $friendsModel->checkSub($data['auth_info']['id'], $data['users']['id']);
        $friendsModel->delSub($data['auth_info']['id'], $data['users']['id']);
        $model->decrementCountSubscribers($data['users']['id']);
        $model->decrementCountSubscriptions($data['auth_info']['id']);
        return redirect()->to('/users/view/'.$data['users']['id']);
    }

}