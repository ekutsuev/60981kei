<?php

namespace App\Controllers;

use App\Services\GoogleClient;
use App\Services\IonAuthGoogle;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;
use IonAuth\Libraries\IonAuth;
use App\Models\UsersModel;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 */

class BaseController extends Controller
{
	/**
	 * An array of helpers to be loaded automatically upon
	 * class instantiation. These helpers will be available
	 * to all other controllers that extend BaseController.
	 *
	 * @var array
	 */
	protected $helpers = [];
    protected $ionAuth;
    protected $google_client;

	/**
	 * Constructor.
	 *
	 * @param RequestInterface  $request
	 * @param ResponseInterface $response
	 * @param LoggerInterface   $logger
	 */
	public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
	{
		// Do Not Edit This Line
		parent::initController($request, $response, $logger);

		//--------------------------------------------------------------------
		// Preload any models, libraries, etc, here.
		//--------------------------------------------------------------------
		// E.g.: $this->session = \Config\Services::session();
        $this->google_client = new GoogleClient();
        $this->ionAuth = new IonAuthGoogle();
        //$this->ionAuth = new IonAuth();
	}
    protected function withIon(array $data = [])
    {
        $modelUsers = new UsersModel();
        if ($this->ionAuth->loggedIn())
        {
            if ($this->ionAuth == null)
            {
                return redirect()->to('/auth/logout');
            }
            $data['auth_info'] = array();
            $user = $modelUsers->getUsersByIonId($this->ionAuth->user()->row()->user_id);
            $data['auth_info'] = $user;
        }

        $data['ionAuth'] = $this->ionAuth;
        $data['authUrl'] = $this->google_client->getGoogleClient()->createAuthUrl();
        return $data;
    }
}
