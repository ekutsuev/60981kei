<?php namespace App\Services;

use Google_Client;

class GoogleClient
{
    private $google_client;
    public function __construct()
    {
        $this->google_client = new Google_Client();
        $this->google_client->setClientId('1089300225548-pdqj5nn46s24t3ll5ckmhvs282ibjhjb.apps.googleusercontent.com');
        $this->google_client->setClientSecret('8x5FIW5Q735HGAGpuZb1YMEb');
        $this->google_client->setRedirectUri(base_url().'/auth/google_login');
        $this->google_client->addScope('email');
        $this->google_client->addScope('profile');
    }
    public function getGoogleClient()
    {
        return $this->google_client;
    }

}