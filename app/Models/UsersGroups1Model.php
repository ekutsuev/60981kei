<?php namespace App\Models;
use CodeIgniter\Model;
class UsersGroups1Model extends Model
{
    protected $table = 'users_groups1'; //Таблица, связанная с моделью
    protected $allowedFields = ['id_user', 'id_group', 'date'];

    public function getSubs($id_group = null)
    {
        if (!isset($id_group))
        {
            $builder = $this->select('users_groups1.*, a.name user_name, a.surname user_surname, a.middle_name user_middle_name, a.picture_url user_picture_url')
                ->join('users_info a', 'users_groups1.id_user = a.id');
            $builder = $builder->select('users_groups1.*, b.name group_name')
                ->join('groups1 b', 'users_groups1.id_group = b.id');
        }
        else
        {
            $builder = $this->select('users_groups1.*, a.name user_name, a.surname user_surname, a.middle_name user_middle_name, a.picture_url user_picture_url')
                ->where('id_group = '."$id_group")
                ->join('users_info a', 'users_groups1.id_user = a.id');
            $builder = $builder->select('users_groups1.*, b.name group_name')
                ->where('id_group = '."$id_group")
                ->join('groups1 b', 'users_groups1.id_group = b.id');
        }
        return $builder->findAll();
    }

    public function checkSub($id_group, $id_user)
    {
        if (empty($this->select('users_groups1.*')
            ->where('id_group = '."$id_group".' and id_user = '."$id_user")->first())) return false;
        else return true;
    }

    public function addSub($id_group, $id_user)
    {
        $insertData = [
            'id_group' => $id_group,
            'id_user' => $id_user,
            'date' => date('Y-m-d')
        ];
        $this->insert($insertData);
    }

    public function delSub($id_group, $id_user)
    {
        $data = $this->select('users_groups1.id')->where('id_group = '."$id_group".' and id_user = '."$id_user")->first();

        $this->delete($data['id']);
    }

    public function DeleteUsersGroups1ByGroupId($id_group)
    {
        $connection = $this->select('users_groups1.id')->where(['users_groups1.id_group' => $id_group])->findAll();
        foreach ($connection as $item)
        {
            $this->delete($item['id']);
        }
    }
}