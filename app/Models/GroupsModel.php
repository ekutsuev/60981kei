<?php namespace App\Models;
use CodeIgniter\Model;
use CodeIgniter\Model\UsersModel;
class GroupsModel extends Model
{
    protected $table = 'groups1'; //Таблица, связанная с моделью
    protected $allowedFields = ['name', 'description', 'date', 'owner', 'count_subs', 'picture_url'];
    public function getGroups($id = null)
    {
        $builder = $this->select('groups1.*, users_info.name user_name, users_info.surname user_surname, users_info.middle_name user_middle_name, users_info.count_subscribers, users_info.count_subscriptions, users_info.users_id')->join('users_info', 'groups1.owner = users_info.id');
        if (!isset($id)) {
            return $builder->findAll();
        }
        return $builder->where(['groups1.id' => $id])->first();
    }

    public function getGroupsByName($name = null)
    {
        $builder = $this->select('groups1.*, users_info.name user_name, users_info.surname user_surname, users_info.middle_name user_middle_name, users_info.count_subscribers, users_info.count_subscriptions, users_info.users_id')->join('users_info', 'groups1.owner = users_info.id');
        if ($name == null) {
            return $builder->findAll();
        }
        return $builder->where(['groups1.name' => $name])->first();
    }

    public function getGroupsWithSearch($id = null, $search = '')
    {
        $builder = $this->select('groups1.*, users_info.name user_name, users_info.surname user_surname, users_info.middle_name user_middle_name, users_info.count_subscribers, users_info.count_subscriptions, users_info.users_id')->join('users_info', 'groups1.owner = users_info.id')->like('groups1.name', $search,'both', null, true)->orlike('groups1.description',$search,'both',null,true);
        if (!is_null($id))
        {
            return $builder->where(['groups1.id' => $id])->first();
        }
        return $builder;
    }

    public function incrementCountSubscribers($id)
    {
        $data = $this->select('groups1.id, groups1.count_subs')->where('groups1.id = '."$id")->first();
        $data['count_subs'] = ++$data['count_subs'];
        $this->update($data['id'], $data);
    }

    public function decrementCountSubscribers($id)
    {
        $data = $this->select('groups1.id, groups1.count_subs')->where('groups1.id = '."$id")->first();
        $data['count_subs'] = --$data['count_subs'];
        $this->update($data['id'], $data);
    }

    public function ValidateName($name)
    {
        if (empty($this->select('groups1.name')->where(['groups1.name' => $name])->first())) return true;
        else return false;
    }
}
