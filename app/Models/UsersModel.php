<?php namespace App\Models;
use CodeIgniter\Model;
class UsersModel extends Model
{
    protected $table = 'users_info'; //Таблица, связанная с моделью
    protected $allowedFields = ['name', 'surname', 'middle_name', 'users_id', 'count_subscribers', 'count_subscriptions', 'picture_url'];
    public function getUsers($id = null)
    {
        if (!isset($id))
        {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }

    public function getUsersByIonId($id)
    {
        return $this->where(['users_id' => $id])->first();
    }

    public function getUsersWithIon($id = null, $search = '')
    {
        $builder = $this->select('users_info.*, users.ip_address, users.username, users.email, users.active, users.company, users.phone, users_groups.group_id')->join('users','users_info.users_id = users.id')->join('users_groups', 'users_info.users_id = users_groups.user_id')->like('name', $search,'both', null, true)->orlike('surname',$search,'both',null,true)->orlike('middle_name',$search,'both',null,true)->orlike('email',$search,'both',null,true);
        if (!is_null($id))
        {
            return $builder->where(['users_info.id' => $id])->first();
        }
        return $builder;
    }
    public function regUsersInfo($data, $email)
    {
        $id = (new AuthModel())->getUsersId($email)['id'];
        $insertData = [
            'name' => $data['first_name'],
            'surname' => $data['last_name'],
            'middle_name' => $data['middle_name'],
            'users_id' => $id,
            'count_subscribers' => 0,
            'count_subscriptions' => 0
        ];
        $this->insert($insertData);
    }

    public function incrementCountSubscribers($id)
    {
        $data = $this->select('users_info.id, users_info.count_subscribers')->where('users_info.id = '."$id")->first();
        $data['count_subscribers'] = ++$data['count_subscribers'];
        $this->update($data['id'], $data);
    }

    public function decrementCountSubscribers($id)
    {
        $data = $this->select('users_info.id, users_info.count_subscribers')->where('users_info.id = '."$id")->first();
        $data['count_subscribers'] = --$data['count_subscribers'];
        $this->update($data['id'], $data);
    }

    public function incrementCountSubscriptions($id)
    {
        $data = $this->select('users_info.id, users_info.count_subscriptions')->where('users_info.id = '."$id")->first();
        $data['count_subscriptions'] = ++$data['count_subscriptions'];
        $this->update($data['id'], $data);
    }

    public function decrementCountSubscriptions($id)
    {
        $data = $this->select('users_info.id, users_info.count_subscriptions')->where('users_info.id = '."$id")->first();
        $data['count_subscriptions'] = --$data['count_subscriptions'];
        $this->update($data['id'], $data);
    }
}