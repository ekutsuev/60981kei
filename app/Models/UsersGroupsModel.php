<?php namespace App\Models;
use CodeIgniter\Model;
class UsersGroupsModel extends Model
{
    protected $table = 'users_groups'; //Таблица, связанная с моделью
    public function getGroups($id = null)
    {
        if (!isset($id))
        {
            return $this->findAll();
        }
        return $this->select('users_groups.group_id')->where(['user_id' => $id])->first();
    }
}