<?php namespace App\Models;
use CodeIgniter\Model;
class MessagesModel extends Model
{
    protected $table = 'messages'; //таблица, связанная с моделью
    public function getMessages($id = null)
    {
        if (!isset($id))
        {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }
}