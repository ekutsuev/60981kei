<?php namespace App\Models;
use CodeIgniter\Model;
class AuthModel extends Model
{
    protected $table = 'users'; //Таблица, связанная с моделью
    public function getUsersId($email = null)
    {
        if (!isset($email))
        {
            return $this->findAll();
        }
        return $this->select('id')->where(['email' => $email])->first();
    }
}
