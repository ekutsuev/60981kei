<?php namespace App\Models;
use CodeIgniter\Model;
class FriendsModel extends Model
{
    protected $table = 'friends'; //Таблица, связанная с моделью
    protected $allowedFields = ['id_user_from', 'id_user_to', 'date'];
    protected $primaryKey = 'id';
    public function getFriends($id = null): array
    {
        if (!isset($id))
        {
            return $this->findAll();
        }
        $sort1 = $this->where(['id_user_from' => $id])->findAll();
        $sort2 = $this->where(['id_user_to' => $id])->findAll();
        return array_merge($sort1, $sort2);
    }

    public function getSubscribersWithUsers($id)
    {
        $builder = $this->select('friends.*, a.name name1, a.surname surname1, a.middle_name middle_name1, a.picture_url')
            ->where('id_user_to = '."$id".'')
            ->join('users_info a','friends.id_user_from = a.id');
        $builder = $builder->select('friends.*, b.name name2, b.surname surname2, b.middle_name middle_name2, b.picture_url')
            ->where('id_user_to = '."$id".'')
            ->join('users_info b','friends.id_user_to = b.id');
        return $builder->findAll();
    }

    public function getSubscriptionsWithUsers($id)
    {
        $builder = $this->select('friends.*, a.name name1, a.surname surname1, a.middle_name middle_name1, a.picture_url')
            ->where('id_user_from = '."$id".'')
            ->join('users_info a','friends.id_user_from = a.id');
        $builder = $builder->select('friends.*, b.name name2, b.surname surname2, b.middle_name middle_name2, b.picture_url')
            ->where('id_user_from = '."$id".'')
            ->join('users_info b','friends.id_user_to = b.id');
        return $builder->findAll();
    }

    public function checkSub($id_from, $id_to)
    {
        $builder = $this->select('friends.*')->where('id_user_from = '."$id_from".' and id_user_to = '."$id_to".'');
        return $builder->findAll();
    }

    public function addSub($id_user_from, $id_user_to)
    {
        $insertData = [
            'id_user_from' => $id_user_from,
            'id_user_to' => $id_user_to,
            'date' => date('Y-m-d')
        ];
        $this->insert($insertData);
    }

    public function delSub($id_user_from, $id_user_to)
    {
        $data = $this->select('friends.id')->where('id_user_from = '."$id_user_from".' and id_user_to = '."$id_user_to")->first();

        $this->delete($data['id']);
    }
}