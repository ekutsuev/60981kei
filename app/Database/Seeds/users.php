<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class users extends Seeder
{
    public function run()
    {
        $data = [
            'name' => 'Admin',
            'count_subscribers' => 0,
            'count_subscriptions' => 0,
            "users_id" => 1
        ];
        $this->db->table('users_info')->insert($data);
    }
}