<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Users extends Migration
{
	public function up()
	{
		//users_info
        if (!$this->db->tableexists('users_info'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'name' => array('type' => 'VARCHAR', 'constraint' => '30', 'null' => FALSE),
                'surname' => array('type' => 'VARCHAR', 'constraint' => '20', 'null' => TRUE),
                'middle_name' => array('type' => 'VARCHAR', 'constraint' => '20', 'null' => TRUE),
                'count_subscribers' => array('type' => 'INT', 'null' => FALSE),
                'count_subscriptions' => array('type' => 'INT', 'null' => FALSE),
                'users_id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE)
            ));
            $this->forge->addForeignKey('users_id','users','id','RESTRICT','RESTRICT');
            // create table
            $this->forge->createtable('users_info', TRUE);
        }

        //friends
        if (!$this->db->tableexists('friends'))
        {
            //Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'id_user_from' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'id_user_to' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'date' => array('type' => 'DATE', 'null' => FALSE)
            ));
            $this->forge->addForeignKey('id_user_from','users_info','id','RESTRICT','RESTRICT');
            $this->forge->addForeignKey('id_user_to','users_info','id','RESTRICT','RESTRICT');
            // create table
            $this->forge->createtable('friends', TRUE);
        }

        //messages
        if (!$this->db->tableexists('messages'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'user_id_from' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'user_id_to' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'text' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
                'is_private' => array('type' => 'TINYINT(1)', 'null' => FALSE),
                'date' => array('type' => 'DATE', 'null' => FALSE)
            ));
            $this->forge->addForeignKey('user_id_from','users_info','id','RESTRICT','RESTRICT');
            $this->forge->addForeignKey('user_id_to','users_info','id','RESTRICT','RESTRICT');
            // create table
            $this->forge->createtable('messages', TRUE);
        }

        //groups1
        if (!$this->db->tableexists('groups1'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'name' => array('type' => 'VARCHAR', 'unique' => TRUE, 'constraint' => '50', 'null' => FALSE),
                'description' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE),
                'date' => array('type' => 'DATE', 'null' => FALSE),
                'owner' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'count_subs' => array('type' => 'int', 'null' => FALSE)
            ));
            $this->forge->addForeignKey('owner','users_info','id','RESTRICT','RESTRICT');
            // create table
            $this->forge->createtable('groups1', TRUE);
        }

        //publication_in_group
        if (!$this->db->tableexists('publication_in_group'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'id_user' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'id_group' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'text' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
                'date' => array('type' => 'DATE', 'null' => FALSE)
            ));
            $this->forge->addForeignKey('id_user','users_info','id','RESTRICT','RESTRICT');
            $this->forge->addForeignKey('id_group','groups1','id','RESTRICT','RESTRICT');
            // create table
            $this->forge->createtable('publication_in_group', TRUE);
        }
	}

	public function down()
	{
        $this->forge->droptable('users_info');
        $this->forge->droptable('friends');
        $this->forge->droptable('messages');
        $this->forge->droptable('groups1');
        $this->forge->droptable('publication_in_group');
    }
}
