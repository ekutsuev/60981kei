<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Addpictureurlfield extends Migration
{
	public function up()
	{
        if ($this->db->tableexists('users_info'))
        {
            $this->forge->addColumn('users_info',array(
                'picture_url' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE)
            ));
        }
        if ($this->db->tableexists('groups1'))
        {
            $this->forge->addColumn('groups1',array(
                'picture_url' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE)
            ));
        }
	}

	public function down()
	{
        $this->forge->dropColumn('users_info', 'picture_url');
        $this->forge->dropColumn('groups1', 'picture_url');
	}
}
