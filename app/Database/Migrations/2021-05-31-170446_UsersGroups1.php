<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class UsersGroups1 extends Migration
{
	public function up()
	{
        //users_groups1
        if (!$this->db->tableexists('users_groups1'))
        {
            //Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'id_group' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'id_user' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'date' => array('type' => 'DATE', 'null' => FALSE)
            ));
            $this->forge->addForeignKey('id_group','groups1','id','RESTRICT','RESTRICT');
            $this->forge->addForeignKey('id_user','users_info','id','RESTRICT','RESTRICT');
            // create table
            $this->forge->createtable('users_groups1', TRUE);
        }
	}

	public function down()
	{
        $this->forge->droptable('users_groups1');
	}
}
